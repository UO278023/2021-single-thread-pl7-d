/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>

using namespace cimg_library;
double total;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char *SOURCE_IMG = "bailarina.bmp";
const char *DESTINATION_IMG = "bailarina-blanco-negro.bmp";
const int REPETITIONS = 80;

int main() {
	//for (uint i = 0; i < 20; i++){
		// Open file and object initialization
	CImg<data_t> srcImage;
		try
		{
			srcImage.assign(SOURCE_IMG);
		}catch(CImgIOException &exception){
			perror("The source image file does not exist\n");
			exit(-2);
		}

		data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
		data_t *pRdest, *pGdest, *pBdest;
		data_t *pDstImage; // Pointer to the new image pixels
		uint width, height; // Width and height of the image
		uint nComp; // Number of image components


		/***************************************************
		 * TODO: Variables initialization.
		 *   - Prepare variables for the algorithm
		 *   - This is not included in the benchmark time
		 */
		struct timespec tStart, tEnd;
	    double dElapsedTimeS;
		data_t pLsrc;	//variable Li, para convertir a blanco & negro
		data_t pLdest;	//variable Li', para invertir la imagen.
		

		srcImage.display(); // Displays the source image
		width  = srcImage.width(); // Getting information from the source image
		height = srcImage.height();
		nComp  = srcImage.spectrum(); // source image number of components
					// Common values for spectrum (number of image components):
					//  B&W images = 1
					//	Normal color images = 3 (RGB)
					//  Special color images = 4 (RGB and alpha/transparency channel)


		// Allocate memory space for destination image components
		pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
		if (pDstImage == NULL) {
			perror("Allocating destination image");
			exit(-2);
		}

		// Pointers to the componet arrays of the source image
		pRsrc = srcImage.data(); // pRcomp points to the R component array
		pGsrc = pRsrc + height * width; // pGcomp points to the G component array
		pBsrc = pGsrc + height * width; // pBcomp points to B component array

		// Pointers to the RGB arrays of the destination image
		pRdest = pDstImage;
		pGdest = pRdest + height * width;
		pBdest = pGdest + height * width;


		/***********************************************
		 * TODO: Algorithm start.
		 *   - Measure initial time
		 */
		if(clock_gettime(CLOCK_REALTIME, &tStart) == -1)
		{
			perror("ERROR: clock_gettime\n");
			exit(-1);
		}

		for (int j = 0; j < REPETITIONS; j++)
		{
		 	pLsrc = 0;
		 	pLdest = 0;

		/************************************************
		 * FIXME: Algorithm.
		 * In this example, the algorithm is a components swap
		 *
		 * TO BE REPLACED BY YOUR ALGORITHM
		 */
			for (uint i = 0; i < width * height; i++){
				//Convertir a blanco & negro.
				pLsrc = *(pRsrc + i) * 0.3 + *(pGsrc + i) * 0.59 + *(pBsrc + i) * 0.11;
				
				//Invertir.
				pLdest = 255 - pLsrc;	

				//Asignar los nuevos valores a los pixeles.
				*(pRdest + i) = pLdest;
			}

		}
		nComp=1; //La imagen es blanco y negro.

		/***********************************************
		 * TODO: End of the algorithm.
		 *   - Measure the end time
		 *   - Calculate the elapsed time
		 */
		if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1)
		{
			perror("ERROR: clock_gettime\n");
			exit(-1);
		}

		dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	    dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
		total += dElapsedTimeS;
		printf("Elapsed time    : %f s.\n", dElapsedTimeS);
			
		// Create a new image object with the calculated pixels
		// In case of normal color images use nComp=3,
		// In case of B/W images use nComp=1.
		CImg<data_t> dstImage;
		try
		{
			dstImage.assign(pDstImage, width, height, 1, nComp);
			// Store destination image in disk
			dstImage.save(DESTINATION_IMG); 
		}catch(CImgIOException &exception){
			perror("Unable to create the destination image; MISSING FOLDER\n");
			exit(-2);
		}


		// Display destination image
		dstImage.display();
		
		// Free memory
		free(pDstImage);
	
		printf("Elapsed time    : %f s.\n", total);
		return 0;
	
}
